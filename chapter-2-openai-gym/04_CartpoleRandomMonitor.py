import gym
import time
env = gym.make("CartPole-v0")
env = gym.wrappers.Monitor(env, "recording")

env.reset()
total_reward = 0 
while True:
    obs, reward, done, _ = env.step(env.action_space.sample())
    total_reward += reward
    
    if done:
        break

print("Total reward: {}".format(total_reward))


# TO RUN THIS IN THE CLOUD:

# $ xvfb-run -s "-screen 0 640x480x24" python 04_cartpole_random_monitor.py

# Another way to record your agent's actions is to use ssh X11 forwarding, which uses the ssh ability to tunnel X11 communications between the X11 client (Python code which wants to display some graphical information) and X11 server (software which knows how to display this information and has access to your physical display). In X11 architecture, the client and the server are separated and can work on different machines. To use this approach, you need the following:

# An X11 server running on your local machine. Linux comes with X11 server as a standard component (all desktop environments are using X11). On a Windows machine, you can set up third-party X11 implementations such as open source VcXsrv (available in https://sourceforge.net/projects/vcxsrv/).
# The ability to log in to your remote machine via ssh, passing the –X command-line option: ssh –X servername. This enables X11 tunneling and allows all processes started in this session to use your local display for graphics output.
# Then you can start a program that uses the Monitor class and it will display the agent's actions, capturing the images into a video file.